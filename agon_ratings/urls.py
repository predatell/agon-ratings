from django.urls import re_path

from agon_ratings.views import rate

urlpatterns = [
    re_path(r"^(?P<content_type_id>\d+)/(?P<object_id>\d+)/rate/$", rate, name="agon_ratings_rate"),
]
