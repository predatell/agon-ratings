# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='OverallRating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.IntegerField(db_index=True)),
                ('rating', models.DecimalField(null=True, max_digits=6, decimal_places=1)),
                ('category', models.IntegerField(null=True)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType', on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.IntegerField(db_index=True)),
                ('rating', models.IntegerField()),
                ('timestamp', models.DateTimeField(default=datetime.datetime.now)),
                ('category', models.IntegerField(null=True)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType', on_delete=models.CASCADE)),
                ('overall_rating', models.ForeignKey(related_name='ratings', to='agon_ratings.OverallRating', null=True, on_delete=models.SET_NULL)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='rating',
            unique_together=set([('object_id', 'content_type', 'user', 'category')]),
        ),
        migrations.AlterUniqueTogether(
            name='overallrating',
            unique_together=set([('object_id', 'content_type', 'category')]),
        ),
    ]
